
package VOS;

import java.util.regex.Pattern;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import estructuras.EncadenamientoSeparadoTH;

/**
 *  
 * @Author: Tomas F. Venegas 
 */
public class VOTeatro {
	
	EncadenamientoSeparadoTH<Integer, VOProgramacion> programacionDias = new EncadenamientoSeparadoTH<>(7);
	
	
	@SerializedName("Nombre")
	@Expose
	private String nombre;
	
	@SerializedName("UbicacionGeografica(Lat|Long)")
	@Expose
	private String ubicacionString;
	
	public String getNombre() {
		return nombre;
	}

	public EncadenamientoSeparadoTH<Integer, VOProgramacion> getProgramacionDias() {
		return programacionDias;
	}

	public void setProgramacionDias(
			EncadenamientoSeparadoTH<Integer, VOProgramacion> programacionDias) {
		this.programacionDias = programacionDias;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUbicacionString() {
		return ubicacionString;
	}

	public void setUbicacionString(String ubicacionString) {
		this.ubicacionString = ubicacionString;
	}

	public VOUbicacion getUbicacionR() {
		return ubicacionR;
	}

	public void setUbicacionR(VOUbicacion ubicacionR) {
		this.ubicacionR = ubicacionR;
	}

	public String getFranquiciaString() {
		return franquiciaString;
	}

	public void setFranquiciaString(String franquiciaString) {
		this.franquiciaString = franquiciaString;
	}

	public VOFranquicia getFranquicia() {
		return franquicia;
	}

	public void setFranquicia(VOFranquicia franquicia) {
		this.franquicia = franquicia;
	}

	private VOUbicacion ubicacionR;
	
	@SerializedName("Franquicia")
	@Expose
	private String franquiciaString;
	
	private VOFranquicia franquicia;
	
	
	public VOTeatro() {
	// TODO Auto-generated constructor stub
	}

	public void parse(){
		if(ubicacionString != null && franquiciaString != null && nombre != null){
			String[] datosUbicacion = ubicacionString.split(Pattern.quote("|"));
			ubicacionR = new VOUbicacion();
			ubicacionR.setLatitud(Double.parseDouble(datosUbicacion[0]));
			ubicacionR.setLongitud(Double.parseDouble(datosUbicacion[1]));
			franquicia = new VOFranquicia();
			franquicia.setNombre(franquiciaString);
		}
	}
	
}