package VOS;






import java.util.Arrays;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VOProgramacion {
	
	@SerializedName("teatros")
	@Expose
	public String nombreTeatro;
	
	@Override
	public String toString() {
		return "VOProgramacion [nombreTeatro=" + nombreTeatro + "]";
	}
	@SerializedName("teatro")
	@Expose
	public ListaPeliculas listapeliculas;
	
	
	public  class ListaPeliculas{
		@SerializedName("peliculas")
		@Expose
		public ListaFuciones[] movies;

		@Override
		public String toString() {
			return "ListaPeliculas [movies=" + Arrays.toString(movies) + "]";
		}
			
	}
	public  class ListaFuciones{
		@SerializedName("id")
		@Expose
		public String movieId;
		
		@SerializedName("funciones")
		@Expose
		public HoraFuncion[] listaHoras;

		@Override
		public String toString() {
			return "ListaFuciones [movieId=" + movieId + ", listaHoras="
					+ Arrays.toString(listaHoras) + "]";
		}
	}
	public  class HoraFuncion{
		@SerializedName("hora")
		@Expose
		public String hora;

		public Long lh(){
			return Long.parseLong(hora);
		}
		@Override
		public String toString() {
			return "HoraFuncion [hora=" + hora + "]";
		}
	}
	
}
