package VOS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VORed {

	@SerializedName("Teatro 1")
	@Expose
	public String teatroString;
	public VOTeatro teatro1;
	
	@SerializedName("Teatro 2")
	@Expose
	public String teatro2String;
	public VOTeatro teatro2;
	
	@SerializedName("Tiempo (minutos)")
	@Expose
	public double tiempoMin;


	
	public VORed() {}
	public VOTeatro geTeatro1() {
		return teatro1;
	}

	public void setTeatro1(VOTeatro t1) {
		this.teatro1 = t1;
	}

	public VOTeatro geTeatr2() {
		return teatro2;
	}

	public void setTeatro2(VOTeatro t2) {
		this.teatro2  = t2;
	}
	public double getTiempoMin() {
		return tiempoMin;
	}

	public void setTiempoMin(double tiempoMin) {
		this.tiempoMin = tiempoMin;
	}
	
	public void parse(){
		if(teatroString!=null && !teatroString.equals("")){
			teatro1 = new VOTeatro();
			teatro1.setNombre(teatroString);
		}
		if(teatro2String!=null&& !teatro2String.equals("")){
			teatro1 = new VOTeatro();
			teatro1.setNombre(teatro2String);
		}
	}
	
}
