package Cliente;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import API.IEdge;
import API.ILista;
import API.ISistemaRecomendacion;
import VOS.VOFranquicia;
import VOS.VOGeneroPelicula;
import VOS.VOPelicula;
import VOS.VOPeliculaPlan;
import VOS.VOProgramacion;
import VOS.VOProgramacion.HoraFuncion;
import VOS.VOProgramacion.ListaFuciones;
import VOS.VOProgramacion.ListaPeliculas;
import VOS.VORed;
import VOS.VOTeatro;
import VOS.VOUbicacion;
import VOS.VOUsuario;
import estructuras.EncadenamientoSeparadoTH;
import estructuras.ListaEnlazada;
import estructuras.ListaEnlazadaSimple;
import estructuras.WeightedGraph;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacion {
	
	private VOTeatro teatros[];
	private VOPelicula peliculas[];
	private VORed[] red;
	private VOProgramacion[] prog;
	
	String generos = "Adventure,Animation,Children,Comedy,Fantasy,Romance,Drama,Action,Crime,Thriller,Horror,Mystery,Sci-Fi,Documentary,IMAX,War,Musical,Western,Film-Noir,(no genres listed)";
	
	private WeightedGraph<String, VOTeatro> grafoteatros;
	private ILista<VOUsuario> usuarios;
	
	public SistemaRecomendacionPeliculas(){
		usuarios=new ListaEnlazadaSimple<VOUsuario>();
	}
	
	public VOUsuario getUser(int id){
		for (VOUsuario user:usuarios){
			if (user.getId()==id){
				return user;
			}
		}
		return null;
	}
	EncadenamientoSeparadoTH<Integer, VOPelicula> THPeliculas;
	
	EncadenamientoSeparadoTH<Integer, VOProgramacion[]> THProgramacion = new EncadenamientoSeparadoTH<>(11);
	//llave=id de la pelicula, valor = tabla de hash con el id de la otra pelicula y la similitud
	EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>> THSimilitudes;
	
	EncadenamientoSeparadoTH<String, VOGeneroPelicula> THGeneros;
	@Override
	public ISistemaRecomendacion crearSR() {
		grafoteatros = new WeightedGraph<>(101);
		THPeliculas = new EncadenamientoSeparadoTH<>(97);
		THSimilitudes = new EncadenamientoSeparadoTH<Integer, EncadenamientoSeparadoTH<Integer, Double>>(101);
		THGeneros = new EncadenamientoSeparadoTH<>(13);
		return this;
	}

	@Override
	public boolean cargarTeatros(String ruta) {
		try{
			Gson g = new Gson();
			teatros = null;
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			teatros = g.fromJson(br, VOTeatro[].class);
			br.close();
			for(VOTeatro t : teatros){
				t.parse();
				grafoteatros.agregarVertice(t.getNombre(), t);
			}
		} catch (Exception e) {
			System.out.println("problema cargando teatros");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
		public boolean cargarCartelera(String ruta) {
			try {
				Gson g = new Gson();

				for (int i = 1; i < 6; i++) {

					String rutaV = "./data/programacion/dia"+i+".json";

					BufferedReader br = new BufferedReader(new FileReader(rutaV));
					prog = g.fromJson(br, VOProgramacion[].class);
					THProgramacion.insertar(i, prog);
				}
				completarInformacion();

			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}

	@Override
	public boolean cargarRed(String ruta) {
		try {
			Gson g = new Gson();
			BufferedReader br = new BufferedReader(new FileReader(ruta));
			red = g.fromJson(br, VORed[].class);
			br.close();

			for(VORed r : red){
				r.parse();
				grafoteatros.agregarArco(r.teatroString, r.teatro2String, r.tiempoMin);
				grafoteatros.agregarArco(r.teatro2String, r.teatroString, r.tiempoMin);
			}
			System.out.println("Se carg� correctamente el grafo con los pesos, id origen, id destino");
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean cargarSimilitudes(){
		try {
			String fruta= "./data/simsMatriz.json";
			BufferedReader br = new BufferedReader(	new FileReader(fruta));
			JsonParser parser = new JsonParser();
			JsonElement element = parser.parse(br);
			JsonArray obj = element.getAsJsonArray();
			for(JsonElement elmento : obj){

				EncadenamientoSeparadoTH<Integer, Double> HTActual= new EncadenamientoSeparadoTH(19);
				Integer idPeliSim =new Integer(0) ;
				Integer idPelitemp =new Integer(0) ;

				JsonObject obj2 =elmento.getAsJsonObject();
				Set<Map.Entry<String, JsonElement>> entries = obj2.entrySet();
				for (Map.Entry<String, JsonElement> entry: entries) {
					if(entry.getKey().equals("movieId")){
						idPelitemp =Integer.parseInt(entry.getValue().getAsString());
						THSimilitudes.insertar(idPelitemp, HTActual);
						break;
					}
					else {
						idPelitemp = Integer.parseInt(entry.getKey());
						Double sim;
						if (!entry.getValue().getAsString().equals("NaN")) sim =Double.parseDouble(entry.getValue().getAsString());
						else sim =0.0;
						HTActual.insertar(idPelitemp, sim);						 
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public int sizeMovies() {
		return THPeliculas.darTamanio();
	}

	@Override
	public int sizeTeatros() {
		// TODO Auto-generated method stub
		return grafoteatros.numVertices();
	}
	

	private VOPelicula getPelicula(String movieId) {
		for (VOPelicula e:peliculas){
			if (e.getId()==Integer.parseInt(movieId)){
				return e;
			}
		}
		return null;
	}

	public VOGeneroPelicula getGenero(String genero) {
		VOGeneroPelicula ans=new VOGeneroPelicula();
		ans.setNombre(genero);
		return ans;
	}
	

	@Override
	public ILista<VOPeliculaPlan> PlanPeliculas(VOUsuario usuario, int fecha) {
		ListaEnlazadaSimple<VOPeliculaPlan> ans=new ListaEnlazadaSimple<VOPeliculaPlan>();
		for (VOTeatro e:teatros){
			ListaFuciones[] movies = e.getProgramacionDias().darValor(fecha).listapeliculas.movies;
			for (int i = 0; i < movies.length; i++) {
				VOPelicula pel=getPelicula(movies[i].movieId);
				HoraFuncion[] k=movies[i].listaHoras;
				for (int j = 0; j < movies.length; j++) {
					VOPeliculaPlan temp=new VOPeliculaPlan();
					temp.setDia(fecha);	
					temp.setPelicula(pel);
					temp.setHoraInicio(new Time(k[j].lh()));
					temp.setTeatro(e);
					ans.agregarElementoFinal(temp);
				}
			}
		}
 		return ans;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGenero(VOGeneroPelicula genero, VOUsuario usuario) {
		ListaEnlazadaSimple<VOPeliculaPlan> ans=new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<ListaEnlazadaSimple<VOPeliculaPlan>> dias=new ListaEnlazadaSimple<>();
		for (VOTeatro e:teatros){
			for (int p = 1; p < 7; p++) {
				ListaFuciones[] movies = e.getProgramacionDias().darValor(p).listapeliculas.movies;
				for (int i = 0; i < movies.length; i++) {
					VOPelicula pel=getPelicula(movies[i].movieId);
					boolean gen=false;
					for (String a:pel.getGeneros()){
						if(a.equalsIgnoreCase(genero.getNombre())){
							gen=true;
							break;
						}
					}
					if(gen){
						HoraFuncion[] k=movies[i].listaHoras;
						for (int j = 0; j < movies.length; j++) {
							VOPeliculaPlan temp=new VOPeliculaPlan();
							temp.setDia(p);	
							temp.setPelicula(pel);
							temp.setHoraInicio(new Time(k[j].lh()));
							temp.setTeatro(e);
							if(dias.darElemento(p-1)!=null){
							dias.darElemento(p-1).agregarElementoFinal(temp);
							}
							else{
								dias.agregarElementoFinal(new ListaEnlazadaSimple<VOPeliculaPlan>());
								dias.darElemento(p-1).agregarElementoFinal(temp);
							}
						}
					}
				}
			}
		}
		int best=0;
		for (ListaEnlazadaSimple<VOPeliculaPlan> e:dias){
			if(e.darNumeroElementos()>best){
				best=e.darNumeroElementos();
				ans=e;
			}
		}
 		return ans;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorFranquicia(VOFranquicia franquicia, int fecha, String franja) throws NumberFormatException, ParseException {
		ListaEnlazadaSimple<VOPeliculaPlan> ans=new ListaEnlazadaSimple<>();
		ListaEnlazadaSimple<VOProgramacion> w=teatrosPorFranja(THProgramacion.darValor(fecha), Integer.parseInt(franja));
		for (VOProgramacion e:w){
			if(getTeatro(e.nombreTeatro).getFranquicia().getNombre().equalsIgnoreCase(franquicia.getNombre())){
				ListaFuciones[] movies = e.listapeliculas.movies;
				for (int i = 0; i < movies.length; i++) {
					VOPelicula pel=getPelicula(movies[i].movieId);
						HoraFuncion[] k=movies[i].listaHoras;
						for (int j = 0; j < movies.length; j++) {
							VOPeliculaPlan temp=new VOPeliculaPlan();
							temp.setDia(fecha);	
							temp.setPelicula(pel);
							temp.setHoraInicio(new Time(k[j].lh()));
							temp.setTeatro(getTeatro(e.nombreTeatro));
							ans.agregarElementoFinal(temp);
					}
				}
			}
		}
		return ans;
	}

	private VOTeatro getTeatro(String nombreTeatro) {
		for (VOTeatro e:teatros){
			if (e.getNombre().equals(nombreTeatro))
				return e;
		}
		return null;
	}

	@Override
	public ListaEnlazadaSimple<VOPeliculaPlan> PlanPorGeneroYDesplazamiento(VOGeneroPelicula genero, int fecha) throws ParseException {
		ListaEnlazadaSimple<VOPeliculaPlan> res=new ListaEnlazadaSimple<VOPeliculaPlan>();
		VOProgramacion[] x= THProgramacion.darValor(fecha);
		ListaEnlazadaSimple<VOProgramacion> w= teatrosPorFranja(x, 1);
		ListaEnlazadaSimple<VOProgramacion> w2= teatrosPorFranja(x, 2);
		ListaEnlazadaSimple<VOProgramacion> w3= teatrosPorFranja(x, 3);
		return res;
	}

	@Override
	public ILista<VOPeliculaPlan> PlanPorGeneroDesplazamientoYFranquicia(VOGeneroPelicula genero, int fecha,
			VOFranquicia franquicia) throws Exception {
		ListaEnlazadaSimple<VOPeliculaPlan> ans= new ListaEnlazadaSimple<VOPeliculaPlan>();
		ListaEnlazadaSimple<VOPeliculaPlan> a = PlanPorGeneroYDesplazamiento(genero, fecha);
		for(VOPeliculaPlan e:a){
			if (e.getTeatro().getFranquicia().getNombre().equals(franquicia)){
				ans.agregarElementoFinal(e);
			}
		}
		return ans;
	}

	@Override
	public ILista<IEdge<VOTeatro>> generarMapa() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<ILista<VOTeatro>> rutasPosible(VOTeatro origen, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	private void completarInformacion(){
		for (int i = 1; i < 6; i++) {
			VOProgramacion[] pro = THProgramacion.darValor(i);
			for (int j = 0; j < pro.length; j++) {
				VOProgramacion a = pro[j];
				for (int k = 0; k < teatros.length; k++)
					if(a.nombreTeatro.equals(teatros[k].getNombre()))
						teatros[k].getProgramacionDias().insertar(i, a);
			}
		}
	}
	
	/**
	 * La franja 1 presenta la ma�ana (8 - 12), la franja 2 representa la tarde(12 - 6) y la franja 3 la noche (6 - 12 pm)
	 * @return
	 * @throws ParseException 
	 */
	private ListaEnlazadaSimple<VOProgramacion> teatrosPorFranja(VOProgramacion[] dia, int f) throws ParseException{
		SimpleDateFormat format = new SimpleDateFormat("HH:mm a");
		
		
		if(f == 1){
			ListaEnlazadaSimple<VOProgramacion> retorno = new ListaEnlazadaSimple<>();
			for (int i = 0; i < dia.length; i++) {
				for (int j = 0; j < dia[i].listapeliculas.movies.length; j++) {
					for (int j2 = 0; j2 < dia[i].listapeliculas.movies[j].listaHoras.length; j2++) {
						String horaS = dia[i].listapeliculas.movies[j].listaHoras[j2].hora;
						horaS = horaS.replace('a', 'A');
						horaS = horaS.replace('p', 'P');
						horaS = horaS.substring(0,horaS.indexOf('.'))+"M";
						if(horaS.contains("P")){
							continue;
						}else{
							Date hora = format.parse(horaS);
							int horadeldia = hora.getHours();
							if(horadeldia < 12 && horadeldia >= 8){
								retorno.agregarElementoFinal(dia[i]);
							}
						}
					}
				}
			}
			
			return retorno;
		}
		if(f == 2){
			ListaEnlazadaSimple<VOProgramacion> retorno = new ListaEnlazadaSimple<>();
			for (int i = 0; i < dia.length; i++) {
				for (int j = 0; j < dia[i].listapeliculas.movies.length; j++) {
					for (int j2 = 0; j2 < dia[i].listapeliculas.movies[j].listaHoras.length; j2++) {
						String horaS = dia[i].listapeliculas.movies[j].listaHoras[j2].hora;
						horaS = horaS.replace('a', 'A');
						horaS = horaS.replace('p', 'P');
						horaS = horaS.substring(0,horaS.indexOf('.'))+"M";
						if(horaS.contains("A")) continue;
						else{
							Date hora = format.parse(horaS);
							int horadeldia = hora.getHours();
							if(horadeldia > 6 && horadeldia <= 12){
								retorno.agregarElementoFinal(dia[i]);
							}
						}
					}
				}
			}
			
			return retorno;
		}
		if(f == 3){
			ListaEnlazadaSimple<VOProgramacion> retorno = new ListaEnlazadaSimple<>();
			for (int i = 0; i < dia.length; i++) {
				for (int j = 0; j < dia[i].listapeliculas.movies.length; j++) {
					for (int j2 = 0; j2 < dia[i].listapeliculas.movies[j].listaHoras.length; j2++) {
						String horaS = dia[i].listapeliculas.movies[j].listaHoras[j2].hora;
						horaS = horaS.replace('a', 'A');
						horaS = horaS.replace('p', 'P');
						horaS = horaS.substring(0,horaS.indexOf('.'))+"M";
						if(horaS.contains("A"))
							continue;
						else{
							Date hora = format.parse(horaS);
							int horadeldia = hora.getHours();
							if(horadeldia >= 6 && horadeldia < 12){retorno.agregarElementoFinal(dia[i]);}
						}
					}
				}
			}
			
			return retorno;
		}
		return null;
	}
	
	public void cargarGeneros()
	{
		String[] gen = generos.split(",");
		for(String actual: gen)
		{
			VOGeneroPelicula agregar = new VOGeneroPelicula();
			agregar.setNombre(actual);
			THGeneros.insertar(actual, agregar);
		}
	}
	
	public VOGeneroPelicula buscarGenero(String genero)
	{
		return THGeneros.darValor(genero);
	}

	public VOFranquicia getFranquicia(String franquicia) {
		VOFranquicia ans=new VOFranquicia();
		ans.setNombre(franquicia);
		return ans;
	}
}
